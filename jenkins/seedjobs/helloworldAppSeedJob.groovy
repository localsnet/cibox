def gitUrl = 'http://gitlab.dfu.mydev.org/babishkevich/hello-world-servlet'
def gitUrlSelenium = 'http://gitlab.dfu.mydev.org/babishkevich/Selenium'



createCiJob("helloworld-app", gitUrl, "app/pom.xml")
createSonarJob("helloworld-app", gitUrl, "app/pom.xml")
createDockerBuildJob("helloworld-app", "app")
createDockerStartJob("helloworld-app", "app", "48080")
createSeleniumJob("helloworld-app", "app", gitUrlSelenium, "pom.xml")
// createDockerPushJob("helloworld-app", "app")
createAnsibleDeployJob("helloworld-app", "app")


def createCiJob(def jobName, def gitUrl, def pomFile) {
    
  println "############################################################################################################"
  println "Creating CI Maven-Nexus Job for ${jobName} "
  println "############################################################################################################"
    
  job("${jobName}-1-ci") {
    parameters {
      stringParam("BRANCH", "master", "Define TAG or BRANCH to build from")
      stringParam("REPOSITORY_URL", "http://nexus:8081/repository/maven-releases/", "Nexus Release Repository URL")
    }
    scm {
      git {
        remote {
          url(gitUrl)
        }
        extensions {
          cleanAfterCheckout()
        }
      }
    }
    wrappers {
      colorizeOutput()
      preBuildCleanup()
    }
    triggers {
      scm('30/H * * * *')
      githubPush()
    }
    steps {
      maven {
          goals('clean versions:set -DnewVersion=DEV-\${BUILD_NUMBER}')
          mavenInstallation('Maven 3.3.3')
          rootPOM( pomFile )
          mavenOpts('-Xms512m -Xmx1024m')
          providedGlobalSettings('bc30ebe0-68e1-4fa7-ab30-38092113a63c')
      }
      maven {
        goals('clean deploy')
        mavenInstallation('Maven 3.3.3')
        rootPOM(pomFile)
        mavenOpts('-Xms512m -Xmx1024m')
        providedGlobalSettings('bc30ebe0-68e1-4fa7-ab30-38092113a63c')
      }
    }
    publishers {
      archiveXUnit {
        jUnit {
          pattern('**/target/surefire-reports/*.xml')
          skipNoTestFiles(true)
          stopProcessingIfError(true)
        }
      }
      publishCloneWorkspace('**', '', 'Any', 'TAR', true, null)
      downstreamParameterized {
        trigger("${jobName}-2-sonar") {
          parameters {
            currentBuild()
          }
        }
      }
    }
  }
}

def createSonarJob(def jobName, def gitUrl, def pomFile) {
    
  println "############################################################################################################"
  println "Creating Sonar Testing Job for ${jobName} "
  println "############################################################################################################"
    
  job("${jobName}-2-sonar") {
    parameters {
      stringParam("BRANCH", "master", "Define TAG or BRANCH to build from")
    }
    scm {
      cloneWorkspace("${jobName}-1-ci")
    }
    wrappers {
      colorizeOutput()
      preBuildCleanup()
    }
    steps {
      maven {
        goals('org.jacoco:jacoco-maven-plugin:0.7.4.201502262128:prepare-agent install -Psonar')
        mavenInstallation('Maven 3.3.3')
        rootPOM(pomFile)
        mavenOpts('-Xms512m -Xmx1024m')
        providedGlobalSettings('bc30ebe0-68e1-4fa7-ab30-38092113a63c')
      }
      maven {
        goals('sonar:sonar -Psonar')
        mavenInstallation('Maven 3.3.3')
        rootPOM(pomFile)
        mavenOpts('-Xms512m -Xmx1024m')
        providedGlobalSettings('bc30ebe0-68e1-4fa7-ab30-38092113a63c')
      }
    }
    publishers {
      downstreamParameterized {
        trigger("${jobName}-3-docker-build") {
          parameters {
            currentBuild()
          }
        }
      }
    }
  }
}

def createDockerBuildJob(def jobName, def folder) {

  println "############################################################################################################"
  println "Creating Docker Build Job for ${jobName} "
  println "############################################################################################################"

  job("${jobName}-3-docker-build") {
    logRotator {
        numToKeep(10)
    }
    scm {
      cloneWorkspace("${jobName}-1-ci")
    }
    steps {
      steps {
        shell("cd ${folder} && sudo /usr/bin/docker build -t helloworld-${folder} .")
      }
    }
    publishers {
      downstreamParameterized {
        trigger("${jobName}-4-docker-start-container") {
          parameters {
            currentBuild()
          }
        }
      }
    }
  }
}

def createDockerStartJob(def jobName, def folder, def port) {

  println "############################################################################################################"
  println "Creating Docker Start Job for ${jobName} "
  println "############################################################################################################"

  job("${jobName}-4-docker-start-container") {
    logRotator {
        numToKeep(10)
    }
    steps {
      steps {
        shell('echo "Stopping Docker Container first"')
        shell("sudo /usr/bin/docker stop \$(sudo /usr/bin/docker ps -a -q --filter=\"name=helloworld-${folder}\") | true ")
        shell("sudo /usr/bin/docker rm \$(sudo /usr/bin/docker ps -a -q --filter=\"name=helloworld-${folder}\") | true ")
        shell('echo "Starting Docker Container"')
        shell("sudo /usr/bin/docker run -d --name helloworld-${folder} -p=${port}:8080 --network=cibox_mynetwork helloworld-${folder}")
      }
    }
   publishers {
downstreamParameterized {
        trigger("${jobName}-5-selenium") {
          parameters {
            currentBuild()
          }
        }
      }
    }
  }
}

def createSeleniumJob(def jobName, def folder, def gitUrlSelenium, def pomFile) {
    
  println "############################################################################################################"
  println "Creating Selenium Testing Job for ${jobName} "
  println "############################################################################################################"
    
  job("${jobName}-5-selenium") {
    parameters {
      choiceParam('app_host', ['http://172.26.24.141:48080/helloworld', 'https://www.google.com'])
      choiceParam('browser_host', ['http://firefox:4444/wd/hub', 'http://chrome:4444/wd/hub'])
     
    }
    scm {
      git {
        remote {
          url(gitUrlSelenium)
        }
        extensions {
          cleanAfterCheckout()
        }
      }
    }
    wrappers {
      colorizeOutput()
      preBuildCleanup()
    }
    triggers {
      scm('30/H * * * *')
      githubPush()
    }
    steps {
      shell("sudo /usr/bin/docker stop \$(sudo /usr/bin/docker ps -a -q --filter=\"name=firefox\") |true")
      shell("sudo /usr/bin/docker run --rm -d -p 4444:4444 --name=firefox --network=ciboxdemo_mynetwork selenium/standalone-firefox")
      maven {
        goals('clean test')
        mavenInstallation('Maven 3.3.3')
        rootPOM(pomFile)
        mavenOpts('-Xms512m -Xmx1024m')
        providedGlobalSettings('bc30ebe0-68e1-4fa7-ab30-38092113a63c')
      }
      
    }
    steps {
       shell("sudo /usr/bin/docker stop firefox")
       shell("sudo /usr/bin/docker stop \$(sudo /usr/bin/docker ps -a -q --filter=\"name=helloworld-${folder}\") |true")
       shell("sudo /usr/bin/docker rm \$(sudo /usr/bin/docker ps -a -q --filter=\"name=helloworld-${folder}\") |true")
    }
    publishers {
      archiveJunit('**/target/surefire-reports/*.xml')
      downstreamParameterized {
        trigger("${jobName}-6-ansible-push-and-deploy") {
          parameters {
            currentBuild()
          }
        }
      }
      
    }
  }
}



/**def createDockerPushJob(def jobName, def folder) {

  println "############################################################################################################"
  println "Creating Docker Push Job for ${jobName} "
  println "############################################################################################################"

  job("${jobName}-6-docker-push") {
    logRotator {
        numToKeep(10)
    }
    steps {
      steps {
        shell("sudo /usr/bin/docker stop \$(sudo /usr/bin/docker ps -a -q --filter=\"name=helloworld-${folder}\") |true")
        shell("sudo /usr/bin/docker rm \$(sudo /usr/bin/docker ps -a -q --filter=\"name=helloworld-${folder}\") |true")
        shell("sudo /usr/bin/docker login -u admin -p admin123 nexus:8083")
        shell("sudo /usr/bin/docker tag helloworld-app:latest nexus:8083/helloworld-app:1")
        shell("sudo /usr/bin/docker push nexus:8083/helloworld-app:1")
      }
    }
 publishers {
downstreamParameterized {
        trigger("${jobName}-7-ansible-deploy") {
          parameters {
            currentBuild()
          }
        }
      }
    }
  }
}
*/

def createAnsibleDeployJob(def jobName, def folder) {

  println "############################################################################################################"
  println "Ansible Deploying Docker Container Job for ${jobName} "
  println "############################################################################################################"

  job("${jobName}-6-ansible-push-and-deploy") {
    logRotator {
        numToKeep(10)
    }
      steps {
          ansibleTower()
      }

  }


buildPipelineView('Pipeline') {
    filterBuildQueue()
    filterExecutors()
    title('Helloworld App CI Pipeline')
    displayedBuilds(5)
    selectedJob("helloworld-app-1-ci")
    alwaysAllowManualTrigger()
    refreshFrequency(60)
}

listView('Helloworld App') {
    description('')
    filterBuildQueue()
    filterExecutors()
    jobs {
        regex(/helloworld-app-.*/)
    }
    columns {
        status()
        buildButton()
        weather()
        name()
        lastSuccess()
        lastFailure()
        lastDuration()
    }
}
}
