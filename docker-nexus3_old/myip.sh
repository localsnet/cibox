#!/bin/sh
getip=`egrep -v "0.0.0.0 | 127." /proc/net/fib_trie | grep -Eohm4 '[1-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | tail -n1`
hosts_orig=/opt/dockerhost/hosts
hosts_temp=/tmp/hosts.old
hostname=nexus

#1 find a nexus string in hosts file
if
 grep -q nexus "$hosts_orig"
#2 replace entire line
then
#workaround without -i on target hosts file, it can't change file's inode
 cp $hosts_orig $hosts_temp
 sed -i '/nexus/c\'"${getip} ${hostname}" "$hosts_temp"
 cp -f $hosts_temp $hosts_orig
 rm -f $hosts_temp

#3 add to last line
else
 echo "$getip $hostname" >> "$hosts_orig"
fi

